#! /usr/bin/env bash

chosen=$(echo -en " Power Off\0icon\x1fsystem-shutdown\n Restart\0icon\x1fsystem-reboot\n Logout\0icon\x1fsystem-log-out\n Sleep\0icon\x1fsystem-suspend\n Lock Screen\0icon\x1flock" | rofi -dmenu -i -show-icons -yoffset -50)

case "$chosen" in
	" Power Off") poweroff ;;
	" Restart") reboot ;;
    " Logout") echo 'awesome.quit()' | awesome-client ;;
	" Sleep") systemctl suspend ;;
    " Lock Screen") light-locker-command -l ;;
	*) exit 1 ;;
esac
