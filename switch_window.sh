#!/usr/bin/bash
xdotool key --delay 300 Alt+Tab&
rofi -show window -yoffset -120 -kb-accept-entry "!Alt-Tab,!Alt+ISO_Left_Tab,Return" -kb-row-down "Alt+Tab" -kb-row-up "Alt+ISO_Left_Tab"&
