#!/bin/bash

function timer() {
		init_time="$(date +%s)"
		working_time=25
		break_time=5
		notification_status=''

		if [ "$1" = 'init' ] || [ "$1" = 'start' ]; then
				prefix="W "
		elif [ "$1" = 'break' ]; then
				prefix="B "
		fi

		suffix=""
		while :
		do
				current_time="$(date +%s)"
				time=$((current_time-init_time))
				if [ "$prefix" = "W " ] && ((time >= working_time*60)); then
						suffix="  passed"
						if ((time%5==0)); then
								paplay "/usr/share/sounds/freedesktop/stereo/complete.oga" &
						fi
						if [ "$notification_status" = '' ] || [ "$notification_status" = 'B' ]; then
								notify-send -u critical 'Enough work! Take a break'
								notification_status="W"
						fi
				elif [ "$prefix" = "B " ] && ((time >= break_time*60)); then
						suffix="  passed"
						if ((time%5==0)); then
								paplay "/usr/share/sounds/freedesktop/stereo/complete.oga" &
						fi
						if [ "$notification_status" = '' ] || [ "$notification_status" = 'W' ]; then
								notify-send -u critical 'Enough rest! Get back to work.'
								notification_status="B"
						fi
				fi
				printf "$prefix""%02d:%02d""$suffix" $((time/60%60)) $((time%60))
				echo
				sleep 1
		done
}

if [ "$1" = 'init' ]; then
		timer init > /tmp/bamador.time &
		echo $! > /tmp/pomo-id
fi

if [ "$1" = 'stop' ]; then
		rm /tmp/bamador.time
		killall bamador
fi

if [ "$1" = 'status' ]; then
		tail -n 1 /tmp/bamador.time
fi

if [ "$1" = 'start' ]; then
		if [ -f "/tmp/bamador.time" ]; then
				rm /tmp/bamador.time
		fi
		kill $(cat /tmp/pomo-id)
        timer start > /tmp/bamador.time &
		echo $! > /tmp/pomo-id
fi

if [ "$1" = 'break' ]; then
		if [ -f "/tmp/bamador.time" ]; then
				rm /tmp/bamador.time
		fi
		kill $(cat /tmp/pomo-id)
        timer break > /tmp/bamador.time &
		echo $! > /tmp/pomo-id
fi
