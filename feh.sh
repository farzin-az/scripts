#!/bin/bash
num=$(cat number)
if [ $num -gt 0 ]; then
		feh --bg-scale $(cat data)
		echo $(expr $num - 1) > number
else
		wallpapers=($HOME/Pictures/Wallpapers/*)
		chosen=$(expr $RANDOM % ${#wallpapers[@]} + 1)
		echo ${wallpapers[$chosen]} > data
		feh --bg-scale $(cat data)
		echo 3 > number
fi

